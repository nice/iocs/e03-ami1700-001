# This should be a test startup script
require ami1700
require essioc

# -----------------------------------------------------
# Configure Environment
# -----------------------------------------------------
# Default paths to locate database and protocol
# epicsEnvSet("EPICS_DB_INCLUDE_PATH", "$(ami1700_DIR)db/")
epicsEnvSet("IP",          "172.30.32.53")
epicsEnvSet("STREAM_NAME", "stream-01")
epicsEnvSet("PORT",        "7180")
epicsEnvSet("P",           "SE-SEE:")
epicsEnvSet("R",           "SE-AMILVL-001:")
epicsEnvSet("ami1700_DB",  "$(ami1700_DIR)db")
epicsEnvSet(LOCATION,   "$(P); $(IP)")
epicsEnvSet(IOCNAME,    "SE-SEE:SE-AMILVL-001")


# E3 Common databases
iocshLoad("$(essioc_DIR)/common_config.iocsh")


iocshLoad("$(ami1700_DIR)/ami1700.iocsh")

#asynSetTraceMask(   stream-01, -1, 0x9)
#asynSetTraceIOMask( stream-01, -1, 0x2)
